<?php
//verification du type de requetes  si un get renvoi le chapitre souhaité en format json 
$METHOD = $_SERVER['REQUEST_METHOD'];
require_once('./lectureRepository.php');

switch($METHOD){

    case "GET":
        header('content-type: application/json; charset=utf-8');
        $response = getOneChapitre();
        echo(json_encode($response));
        break;

    default:
        echo("mauvaise route");
        break;
}