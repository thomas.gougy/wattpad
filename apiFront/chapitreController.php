<?php
//verification du type de requetes  si un get renvoi tout les chapitre en format json 
$METHOD = $_SERVER['REQUEST_METHOD'];
require_once('./chapitreRepository.php');

switch($METHOD){

    case "GET":
        header('content-type: application/json; charset=utf-8');
        $response = getAllChapitre();
        echo(json_encode($response));
        break;

    default:
        echo("mauvaise route");
        break;
}