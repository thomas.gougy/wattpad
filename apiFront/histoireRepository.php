<?php

require_once("./pdo.php");
// recupere toutes les histoires dans la bdd et renvoi la data
function getAllHistoire(){
    global $connectionPDO;
    $request = $connectionPDO->prepare('SELECT * FROM histoire;');
    $request->execute();
    $data = $request->fetchAll(PDO::FETCH_ASSOC);
    return $data;
}
