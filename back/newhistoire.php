<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>creation d'une histoire</title>
    <link rel="stylesheet" href="./css/header.css">
    <link rel="stylesheet" href="./css/new.css">
</head>
<body>
    <?php
    require_once('./PDO.php');
    require_once('./guard.php');
    ?>
    <header>
        <h1> WattEcriture</h1>
        <a href='./histoire.php'><button class='retour'>  retour a la liste d'histoire</button></a>
        <a href='./deco.php'><button class='decon'> Deconnexion </button></a>
    </header>
    <div class=container>
        <div class='new'>
        <form method='POST'>
            <label> Titre de votre histoire :</label>
            <input type='text' class='titre' name='titre'>
            </br>
            <label> Description de votre Histoire :</label>
            <textarea name='description'></textarea>
            </br>
            <input type='submit' class='sub' value="créer l'histoire">
        </form>
        </div>  

</div>
<?php
if(
    //verification si les champs sont vide et si c'est pas le cas l'histoire est envoyé dans la base grace a l'id user recuperé grace a son cookie 
    $_POST["titre"]=="" ||
    $_POST["description"]== ""){
        echo('<script>alert("le titre ou la description est manquant")</script>');

    }else{

    global $connectionPDO;
    $getAuteur = $connectionPDO->prepare('SELECT * FROM `auteur` WHERE token=:idauteur;');
    $getAuteur->execute(["idauteur" => $_COOKIE["wattpad-auth"]]);
        $auteur= $getAuteur->fetch(PDO::FETCH_ASSOC);
    $createhistoire = $connectionPDO->prepare('INSERT INTO `histoire`(`idlivre`, `idauteur`, `nomhistoire`, `description`) VALUES ( :idlivre, :auteur, :titre, :description);');
    $createhistoire->execute(["idlivre"=> V4(), "auteur" => $auteur["idauteur"], "titre"=> $_POST["titre"], "description"=> $_POST["description"]]); 
    header('Location: ./histoire.php');
    
    
}

?>
    
</body>
</html>