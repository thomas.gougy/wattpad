<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>creation chapitre</title>
    <link rel="stylesheet" href="./css/header.css">
    <link rel="stylesheet" href="./css/new.css">
</head>
<body>
    <?php
    require_once('./PDO.php');
    require_once('./guard.php');
    //recuperation de l'id livre grace a l'url
    $url = $_SERVER['REQUEST_URI'];
    $requetes = str_replace("idlivre=","",parse_url($url, PHP_URL_QUERY));
    
    ?>
    <header>
        <h1> WattEcriture</h1>
        <a <?php echo('href="./chapitre.php?idlivre='.$requetes.'"')?>> <button class='retour'>  retour a la liste des chapitres</button></a>
        <a href='./deco.php'><button class='decon'> Deconnexion </button></a>
    </header>
    <div class=container>
        <div class='new'>
        <form method='POST'>
            <label> Titre de votre chapitre :</label>
            <input type='text' class ='titre'name='titre'>
            </br>
            <label> contenue de votre chapitre :</label>
            <textarea name='chapitre'></textarea>
            </br>
            <input type='submit' class='sub' value="créer le chapitre">
        </form>
        </div>

    </div>
<?php
if(
    //verification que les champs soit vides et ensuite mise du chapitre dansz la bdd
    $_POST["titre"]=="" ||
    $_POST["chapitre"]== ""){
        echo('<script>alert("le titre ou la description est manquante")</script>');

    }else{
    global $connectionPDO;
    $createchapitre = $connectionPDO->prepare('INSERT INTO `chapitre`(`idchapitre`, `idlivre`, `nomchapitre`, `chapitre`) VALUES (:idchapitre , :idlivre , :titre, :chapitre);');
    $createchapitre->execute(["idchapitre"=> V4(), "idlivre" => $requetes, "titre"=> $_POST["titre"], "chapitre"=> $_POST["chapitre"]]); 
    header('Location: ./chapitre.php?idlivre='.$requetes.'');
    
    
}

?>
    
</body>
</html>