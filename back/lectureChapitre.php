<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>lecture du chapitre</title>
    <link rel="stylesheet" href="./css/header.css">
    <link rel="stylesheet" href="./css/lire.css">
</head>
<body>
    <?php

      require_once('./PDO.php');
      require_once('./guard.php');
      //recupere l'id du chapitre dans l'url est recupere les information dans la base grace a sa 
      $url = $_SERVER['REQUEST_URI'];
      $requetes = str_replace("idchapitre=","",parse_url($url, PHP_URL_QUERY));

      $getChapitre = $connectionPDO->prepare('SELECT * FROM `chapitre` WHERE idchapitre LIKE :id ;');
      $getChapitre->execute(["id" => $requetes]);
      $chapitre = $getChapitre->fetch(PDO::FETCH_ASSOC);
      ?>


<header>

<h1> WattEcriture</h1>
<a href='./chapitre.php?idlivre=<?php echo($chapitre["idlivre"])   ?>'><button class='retour'>Retour a la liste des chapitres</button></a>
<a href='./deco.php'><button class='decon'> Deconnexion </button></a>
</header>
<div class='container'>
    <div class='chapitre'>
    <?php
    echo("<h1>".$chapitre['nomchapitre']."</h1>");
    echo("<p>".$chapitre['chapitre']."</h1>");
    ?>
    </div>


</div>
</body>
</html>