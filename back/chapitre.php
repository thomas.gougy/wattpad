<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>chapitres</title>
    <link rel="stylesheet" href="./css/header.css">
    <link rel="stylesheet" href="./css/chapitre.css">

</head>
<body>
<?php 
require_once('./PDO.php');
require_once('./guard.php');
//recuperation de l'url afin de recuperer l'id de l'element 
$url = $_SERVER['REQUEST_URI'];
$requetes = str_replace("idlivre=","",parse_url($url, PHP_URL_QUERY));

?>
<header>
<h1> WattEcriture</h1>
<a href='./histoire.php' ><button class='retour'>Retour a la liste</button></a>

<a href='./deco.php'><button class='decon'> Deconnexion </button></a>
</header>
    
<div class='container'>
    <div class="histoire">
    
        <?php
        //requetes pour recuperer le livre grace a son id et mise en place d'un espace pour modifier l'histoire
        $getHistoire = $connectionPDO->prepare('SELECT * FROM `histoire` WHERE idlivre LIKE :id ;');
        $getHistoire->execute(["id" => $requetes]);
            $histoire = $getHistoire->fetch(PDO::FETCH_ASSOC);
           $pouet = $histoire['description'];
          
        
        echo("<h1>".$histoire["nomhistoire"]."</h1>");
        echo("<form method='POST'>");
       

        echo("<textarea type ='text' name='modif'>".$pouet."</textarea>");
       
        echo('<input type="submit" id="modif" value="modifier">');
        
        echo("</form>");
       
        

       


        //verification de la modification et update de la base 
        if(
            $_POST["modif"] == ""
        ){
            echo("aucune modification n'a été apportée");
        }else{
            global $connectionPDO;
            $modifHistoire = $connectionPDO->prepare('UPDATE `histoire` SET `description`=:modif WHERE idlivre=:idlivre;');
            $modifHistoire->execute(["modif" => $_POST['modif'], "idlivre"=> $requetes]);

        }
        ?>

    </div>
    <div id="containerChapitre">
        <div id='titreChapitre'>
        <?php 
        //recuperation des chapitres grace a l'url recuperé et generation du listing des chapitres 
        echo("<h1> Liste des chapitres</h1>");
        echo("<a href='./newchapitre.php?idlivre=".$histoire['idlivre']."'><button id='newChap'>Ajouter un chapitre</button></a>");
        echo("</div>");
        echo("<div id='listeChapitre'>");
            global $connectionPDO;
            $getChapitre = $connectionPDO->prepare('SELECT * FROM chapitre WHERE idlivre = "'.$requetes.'" ;');
            $getChapitre->execute();
                $chapitres = $getChapitre->fetchAll(PDO::FETCH_ASSOC);
            
            foreach( $chapitres as $chapitre){
                
                echo("<div class='chapitre'>");
                echo("<h1>".$chapitre["nomchapitre"]."</h1>");
                echo("<div class='buttonChap'>");
                echo("<a href='./lectureChapitre.php?idchapitre=".$chapitre['idchapitre']."'><button class='lecture'>Lire</button></a>");
                echo("<a href='./modifierChapitre.php?idchapitre=".$chapitre['idchapitre']."'><button class='modifer'>modifier</button></a>");
                echo("<a href='./deleteChapitre.php?idchapitre=".$chapitre['idchapitre']."'><button class='delete'>supprimer</button></a>");
                echo("</div>");
                echo("</div>");
        
            }


            ?>



</div>
</body>
</html>