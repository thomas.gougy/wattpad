<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>supprimer un chapitre</title>
    <link rel="stylesheet" href="./css/header.css">
    <link rel="stylesheet" href="./css/delete.css">
</head>
<body>
<?php 
require_once('./PDO.php');
require_once('./guard.php');
//recuperation de l'url afin de recuperer l'id de l'element 
$url = $_SERVER['REQUEST_URI'];
$requetes = str_replace("idchapitre=","",parse_url($url, PHP_URL_QUERY));

?>
<header>
<h1> WattEcriture</h1>
<a href='./histoire.php'><button class='retour'>Retour a la liste</button></a>

<a href='./deco.php'><button class='decon'> Deconnexion </button></a>
</header>

<div class='container'>
    <div class="delete">
        <form method="POST">
            <h2>Souhaitez vous vraiment supprimer votre chapitre ?</h2>
            
            <label> <input type="checkbox" name="verification"> oui je le veux </label>
            <p>Attention le chapitre va etre supprimé definitivement</p>
            <input type='submit' value='supprimer'>
        </form>
    

    <?php
    //delete le chapitre voulu apres avoir verifier que le user veux bien le faire 
    if($_POST["verification"] == true){
    global $connectionPDO;
        $deleteChapitre = $connectionPDO->prepare('DELETE FROM chapitre WHERE idchapitre=:idchapitre ;');
        $deleteChapitre->execute(["idchapitre" => $requetes]);
        header('Location: ./histoire.php');

    
        
    }elseif($_POST["verification"] == false){
        echo("<p>le chapitre n'a pas été supprimé");
    }

    ?>
    </div>

</div>

</body>
</html>