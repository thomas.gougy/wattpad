<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>liste histoire</title>
    <link rel="stylesheet" href="./css/header.css">
    <link rel="stylesheet" href="./css/histoire.css">
</head>
<body>
<?php 
require_once('./PDO.php');
require_once('./guard.php');
?>
<header>
<h1> WattEcriture</h1>
<a href='./newhistoire.php'><button class='create'>créer une nouvelle histoire</button></a>
<a href='./deco.php'><button class='decon'> Deconnexion </button></a>
</header>
<div class="container">
<?php

    //recuperation des histoire selon l'id de l'auteur recupéré grace au cookie et generation de ses histoire
    global $connectionPDO;
    $getAuteur = $connectionPDO->prepare('SELECT * FROM `auteur` WHERE token=:idauteur;');
        $getAuteur->execute(["idauteur" => $_COOKIE["wattpad-auth"]]);
        $auteur= $getAuteur->fetch(PDO::FETCH_ASSOC);
    $getHistoire = $connectionPDO->prepare('SELECT * FROM `histoire` WHERE idauteur LIKE :id ;');
    $getHistoire->execute(["id" => $auteur["idauteur"]]);
        $histoires = $getHistoire->fetchAll(PDO::FETCH_ASSOC);
       
        foreach( $histoires as $histoire){
        echo("<div class='histoire'>");
        echo("<h1>".$histoire["nomhistoire"]."</h1>");
        echo("<p>".$histoire["description"]."</p>");
        echo("<div class='bouttonHistoire'>");
        echo("<a href='./chapitre.php?idlivre=".$histoire['idlivre']."'><button class='chapter'>Liste des chapitres </button></a>");
        echo("<a href='./deletehistoire.php?idlivre=".$histoire['idlivre']."'><button class='suppr'>supprimer l'histoire</button></a>");
        echo("</div>");
        echo("</div>");

    
}

?>


</div>
</body>
</html>