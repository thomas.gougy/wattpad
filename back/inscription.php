<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>inscription</title>
    <link rel="stylesheet" href="./css/header.css">
    <link rel="stylesheet" href="./css/inscription.css">
    
</head>
<body>
<div class='container'>
<?php
require_once('./PDO.php');
?>
<body>
    <div id='connexion'>
        <h1>Inscription</h1>
        <form id='inscription' action='#' method='POST'>
            <label>Email :</label>
            <input type="email" name="email">
            <label>Mot de Passe :</label>
            <input type='password' name="password">
            <label>Nom :</label>
            <input type='text' name='nom'>
            <label>Prenom :</label>
            <input type='text' name='prenom'>
            <label>Pseudo :</label>
            <input type='text' name='pseudo'>

            <input type='submit' value="S'inscrire">
        </form>
        

<?php
    if(
        $_POST["email"] != "" &&
        $_POST["password"] != "" &&
        $_POST["nom"] != "" &&
        $_POST["prenom"] != "" &&
        $_POST["pseudo"] != ""
    ){
        // mise en place d'un regex pour le mdp 
        $getEmail = $connectionPDO->prepare('SELECT * FROM `auteur` WHERE email=:email;');
        $getEmail->execute(["email" => $_POST["email"]]);
        $verifyEmail = $getEmail->fetch(PDO::FETCH_ASSOC);
        $regex = "/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/";
        $passwordConformity = preg_match($regex, $_POST['password']);
    
        if($passwordConformity == false || $passwordConformity == 0){
            
            echo('<script>alert("Votre mots de passe n\'est pas conforme il dois contenir minimum 8 caractere 1 maj une min et un chiffre mais pas de caractere spéciale")</script>');

        }else{

            //verification du mail retourne une alert si l'email est deja utilisé 
            if($verifyEmail){
                echo('<script>alert("Mail deja utilisé")</script>');
            }
            else{
                //verification que le pseudo soit deja utilisé ou pas 
                $getPseudo = $connectionPDO->prepare('SELECT * FROM `auteur` WHERE pseudo=:pseudo;');
                $getPseudo->execute(["pseudo" => $_POST["pseudo"]]);
                $verifyPseudo = $getPseudo->fetch(PDO::FETCH_ASSOC);

                if($verifyPseudo){
                    echo('<script>alert("Pseudo deja utilisé")</script>');
                
                }else{
                    
                    //hash le mot de passe 
                    $hashPassword = mdpHash($_POST["password"],$_POST["email"]);
                    $roles = "user";
                    $generatedId= v4();
                    $requete ='INSERT INTO `auteur`(`idauteur`, `email`, `mdp`, `nom`, `prenom`, `pseudo`, `roles`) VALUES';
                    $requete .='(:id, :email, :mdp, :nom, :prenom , :pseudo,:role);';
                    $createUser = $connectionPDO->prepare($requete);
                    $createUser->execute(["id"=> $generatedId, "email"=> $_POST["email"], "mdp"=> $hashPassword, "nom"=>$_POST["nom"], "prenom"=>$_POST["prenom"], "pseudo"=> $_POST["pseudo"], "role"=>$roles]);
                    
                    
                    header('Location: ./index.php');
                    echo('<script>alert("Votre compte a bien été créé vous pouvez vous connecter")</script>');
                }
            }
        }

    }

    
    


    
?>
</div>
</body>
</html>