<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Connexion</title>
    <link rel="stylesheet" href="./css/header.css">
    <link rel="stylesheet" href="./css/index.css">
</head>
<?php
require_once('./PDO.php');
?>
<body>
<div class="container">
    <div id='connexion'>
        <h1>Connexion</h1>
        <form id='connexion' action='#' method='POST'>
            <label>Email :</label>
            <input type="email" name="email">
            <label>Mot de Passe :</label>
            <input type='password' name="password">
            <input type='submit' value='Se Connecter'>
            <a href='./inscription.php'>s'inscrire </a>
        </form>

    <?php
  if(
    $_POST["email"] != "" &&
    $_POST["password"] != ""

){
// recuperation du user selon l'email indiqué 
    $getEmail = $connectionPDO->prepare('SELECT * FROM `auteur` WHERE email=:email;');
    $getEmail->execute(["email" => $_POST["email"]]);
    $verifyEmail = $getEmail->fetch(PDO::FETCH_ASSOC);
    
    //verification de l'email 
        if(!$verifyEmail){
            echo('<script>alert("Ce mail n\'existe pas")</script>');
        }
        else{
            //recuperation du mdp du user pour voir si il correspond au mot de pass qui a indiqué 
            $getmdp = $connectionPDO->prepare('SELECT * FROM `auteur` WHERE idauteur LIKE :id AND mdp=:password');
            $hashPassword = mdpHash($_POST["password"],$_POST["email"]);
            $getmdp->execute(["password" => $hashPassword, "id" => $verifyEmail["idauteur"]]);
            $verifymdp = $getmdp->fetch(PDO::FETCH_ASSOC);
            if(!$verifymdp){
                echo('<script>alert("ce Password est faux ")</script>');
            }else{
                //creation du token
                connexionToken($verifymdp["idauteur"]);
                //une fois connecté le user est redirigé vers ses histoire
                header('Location: ./histoire.php');
            }
        }
    


 }
 //fonction de creation du token et mise du token dans la BDD
 function connexionToken($idauteur){
    global $connectionPDO;
    $token = v4();
  
    $createToken = $connectionPDO->prepare('UPDATE `auteur` SET token=:token, created=NOW() WHERE idauteur=:id ');
    $createToken->execute([ "token" => $token, "id" => $idauteur ]);
  
    setcookie("wattpad-auth", $token, time() + 3600*2, "/", "", true, true);
  }
  
  function deconnexion(){
    setcookie("wattpad-auth", "null", time() +0, "/", "", true, true);
  }

?>

    </div>
    <style>
    form{
        display: flex;
        justify-content: center;
        flex-direction: column;
    }
    #connexion{
        width: 25%;
        

    }
    </style>
</div>
</body>
</html>