<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./css/header.css">
    <link rel="stylesheet" href="./css/delete.css">
</head>
<body>
<?php 
require_once('./PDO.php');
require_once('./guard.php');
$url = $_SERVER['REQUEST_URI'];
$requetes = str_replace("idlivre=","",parse_url($url, PHP_URL_QUERY));

?>
<header>
<h1> WattEcriture</h1>
<a href='./histoire.php'><button class='retour'>Retour a la liste</button></a>

<a href='./deco.php'><button class='decon'> Deconnexion </button></a>
</header>

<div class='container'>
    <div class="delete">
<form method="POST">
<h2>Souhaitez vous vraiment supprimer votre histoire ?</h2>
<input type="checkbox" name="verification">
<label>oui je le veux </label>
<p>Attention tout les chapitre lié au livre seront supprimé aussi</p>
<input type='submit' value='supprimer'>
</form>

<?php

 //delete l'histoire et ses chapitre voulu apres avoir verifier que le user veux bien le faire 
if($_POST["verification"] == true){
    global $connectionPDO;

    $deleteChapitre = $connectionPDO->prepare('DELETE FROM chapitre WHERE idlivre=:idlivre ;');
    $deleteChapitre->execute(["idlivre" => $requetes]);
   
    

    $deleteHistoire = $connectionPDO->prepare('DELETE FROM histoire WHERE idlivre=:idlivre ;');
    $deleteHistoire->execute(["idlivre" => $requetes]);
    
    header('Location: ./histoire.php');
    
    
}elseif(($_POST["verification"] == false)){
    echo("<p>l'histoire n'a pas été supprimé</p>");
}

?>
</div>

</div>

</body>
</html>